package dev.com.sfilizzola.adventurecompanion.viewmodels

import android.databinding.Bindable
import dev.com.sfilizzola.adventurecompanion.models.Character
import timber.log.Timber

class CharacterRowViewModel constructor(private val item:Character):  BaseViewModel(){

    fun onRowClick(){
        Timber.d("clicked")
    }

    @Bindable
    fun getName():String = item.name

    @Bindable
    fun getRaceClass():String = "Halfling Rogue 1"

    @Bindable
    fun getLevel(): String = "Level 1"
}