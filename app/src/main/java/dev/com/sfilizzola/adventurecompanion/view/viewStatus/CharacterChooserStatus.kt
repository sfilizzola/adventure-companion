package dev.com.sfilizzola.adventurecompanion.view.viewStatus

import dev.com.sfilizzola.adventurecompanion.models.Character

sealed class CharacterChooserStatus {
    data class Success(val list: List<Character>):CharacterChooserStatus()
    data class Error(val error:String?):CharacterChooserStatus()
    object CreateCharacter : CharacterChooserStatus()

    fun list():List<Character>{
        return when(this){
            is Success -> this.list
            else -> ArrayList<Character>()
        }
    }

    fun error():String?{
        return when(this){
            is Error -> this.error
            else -> null
        }
    }
}