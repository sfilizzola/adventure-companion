package dev.com.sfilizzola.adventurecompanion.adapters

import dev.com.sfilizzola.adventurecompanion.R
import dev.com.sfilizzola.adventurecompanion.databinding.CharacterRowBinding
import dev.com.sfilizzola.adventurecompanion.models.Character
import dev.com.sfilizzola.adventurecompanion.viewmodels.CharacterRowViewModel

class CharListAdapter : BaseAdapter<Character, CharacterRowBinding>(R.layout.character_row) {


    override fun areItemsTheSame(oldItem: Character, newItem: Character): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Character, newItem: Character): Boolean {
        return oldItem.id == newItem.id
    }

    override fun bind(holder: DataBindViewHolder<CharacterRowBinding>, position: Int) {
        holder.binding.viewModel = CharacterRowViewModel(items[position])
    }

}