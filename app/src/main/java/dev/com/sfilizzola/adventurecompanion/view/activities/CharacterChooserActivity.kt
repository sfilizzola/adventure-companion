package dev.com.sfilizzola.adventurecompanion.view.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import dev.com.sfilizzola.adventurecompanion.databinding.ActivityCharacterChooseBinding
import dev.com.sfilizzola.adventurecompanion.R
import dev.com.sfilizzola.adventurecompanion.adapters.CharListAdapter
import dev.com.sfilizzola.adventurecompanion.view.viewStatus.CharacterChooserStatus
import dev.com.sfilizzola.adventurecompanion.viewmodels.CharacterChooserViewModel
import javax.inject.Inject

class CharacterChooserActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(CharacterChooserViewModel::class.java) }

    private lateinit var binding: ActivityCharacterChooseBinding
    private lateinit var charAdapter: CharListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_character_choose)
        binding.viewModel = viewModel

        charAdapter = CharListAdapter()

        with(binding.characterList) {
            this.setHasFixedSize(true)
            this.adapter = charAdapter
            this.layoutManager = LinearLayoutManager(context)
        }

        viewModel.getData().observe(this, Observer {
            it?.let { result ->
                when (result) {
                    is CharacterChooserStatus.Success -> charAdapter.update(it.list())
                    is CharacterChooserStatus.Error -> { displaySnackbarMessage(binding.root, it.error()!!, null)
                    }
                    is CharacterChooserStatus.CreateCharacter -> {
                        val intent = Intent(this, NewCharacterBasicActivity::class.java)
                        startActivity(intent)
                    }

                }

            }
        })
    }


    override fun onResume() {
        super.onResume()
        viewModel.fetch()
    }
}