package dev.com.sfilizzola.adventurecompanion.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.databinding.Bindable
import android.view.View
import dev.com.sfilizzola.adventurecompanion.BR

import dev.com.sfilizzola.adventurecompanion.models.Character
import dev.com.sfilizzola.adventurecompanion.repos.CharListRepo
import dev.com.sfilizzola.adventurecompanion.view.viewStatus.CharacterChooserStatus
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class CharacterChooserViewModel @Inject constructor(var repo: CharListRepo) : BaseViewModel() {

    private var data = MutableLiveData<CharacterChooserStatus>()
    private var currentData: List<Character> = ArrayList()

    fun fetch() {
        compositeDisposable.add(repo.fetchCharList().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    data.postValue(CharacterChooserStatus.Success(it))
                    currentData = it
                    notifyPropertyChanged(BR.disclaimerVisibility)
                    notifyPropertyChanged(BR.recyclerVisibility)
                }, {
                    data.postValue(CharacterChooserStatus.Error(it.message))
                    Timber.e(it)
                }))
    }

    fun getData(): LiveData<CharacterChooserStatus> = data

    fun onAddClick() {
        data.postValue(CharacterChooserStatus.CreateCharacter)
    }

    @Bindable
    fun getRecyclerVisibility(): Int = if (currentData.isEmpty()) View.GONE else View.VISIBLE

    @Bindable
    fun getDisclaimerVisibility(): Int = if (currentData.isNotEmpty()) View.GONE else View.VISIBLE


}