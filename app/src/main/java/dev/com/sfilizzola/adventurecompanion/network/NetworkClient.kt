package dev.com.sfilizzola.adventurecompanion.network

import dev.com.sfilizzola.adventurecompanion.network.networkModels.RaceResponse
import dev.com.sfilizzola.adventurecompanion.network.networkModels.ListResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path


interface NetworkClient {

    @GET("races/")
    fun listRaces(): Single<ListResponse>

    @GET("classes/")
    fun listClasses(): Single<ListResponse>

    @GET("races/{id}")
    fun getRace(@Path("id") raceId:String):Single<RaceResponse>
}