package dev.com.sfilizzola.adventurecompanion.view.fragments

import dagger.android.support.DaggerFragment


open class BaseFragment : DaggerFragment() {

}