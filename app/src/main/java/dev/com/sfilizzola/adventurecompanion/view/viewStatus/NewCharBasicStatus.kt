package dev.com.sfilizzola.adventurecompanion.view.viewStatus
import dev.com.sfilizzola.adventurecompanion.models.ListItem
import dev.com.sfilizzola.adventurecompanion.models.Race

sealed class NewCharBasicStatus {

    data class RaceListLoaded(val list: List<ListItem>):NewCharBasicStatus()
    data class RaceLoaded(val race: Race):NewCharBasicStatus()
    data class Error(val error:String?):NewCharBasicStatus()
    object NextStep : NewCharBasicStatus()

    fun list():List<ListItem>{
        return when(this){
            is NewCharBasicStatus.RaceListLoaded -> this.list
            else -> ArrayList()
        }
    }


    fun race():Race?{
        return when(this){
            is NewCharBasicStatus.RaceLoaded -> this.race
            else -> null
        }
    }


    fun error():String?{
        return when(this){
            is NewCharBasicStatus.Error -> this.error
            else -> null
        }
    }

}