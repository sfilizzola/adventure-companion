package dev.com.sfilizzola.adventurecompanion.network.networkModels

import com.google.gson.annotations.SerializedName

data class RaceResponse(var _id: String,
                        var index: Int,
                        var name: String,
                        var speed: Int,
                        @SerializedName("ability_bonuses")
                        var abilityBonuses: ArrayList<Int>,
                        var alignment: String,
                        var age: String,
                        var size: String,
                        @SerializedName("size_description")
                        var sizeDesc: String,
                        @SerializedName("starting_proficiencies")
                        var startingProficiences: ArrayList<ListResponse>,
                        var languages: ArrayList<ListResponse>,
                        var traits: ArrayList<ListResponse>,
                        var subraces: ArrayList<ListResponse>,
                        @SerializedName("language_desc")
                        var langDesc: String,
                        var url:String)


/*
{
	"_id": "5a52baf5559f00418e532721",
	"index": 1,
	"name": "Dwarf",
	"speed": 30,
	"ability_bonuses": [
		0,
		0,
		2,
		0,
		0,
		0
	],
	"alignment": "Most dwarves are lawful, believing firmly in the benefits of a well-ordered society. They tend toward good as well, with a strong sense of fair play and a belief that everyone deserves to share in the benefits of a just order.",
	"age": "Dwarves mature at the same rate as humans, but they’re considered young until they reach the age of 50. On average, they live about 350 years.",
	"size": "Medium",
	"size_description": "Dwarves stand between 4 and 5 feet tall and average about 150 pounds. Your size is Medium.",
	"starting_proficiencies": [
		{
			"name": "Battleaxes",
			"url": "http://www.dnd5eapi.co/api/proficiencies/20"
		},
		{
			"name": "Handaxes",
			"url": "http://www.dnd5eapi.co/api/proficiencies/24"
		},
		{
			"name": "Light hammers",
			"url": "http://www.dnd5eapi.co/api/proficiencies/26"
		},
		{
			"name": "Warhammers",
			"url": "http://www.dnd5eapi.co/api/proficiencies/51"
		}
	],
	"starting_proficiency_options": {
		"choose": 1,
		"type": "proficiencies",
		"from": [
			{
				"name": "Smith's tools",
				"url": "http://www.dnd5eapi.co/api/proficiencies/71"
			},
			{
				"name": "Brewer's supplies",
				"url": "http://www.dnd5eapi.co/api/proficiencies/59"
			},
			{
				"name": "Mason's tools",
				"url": "http://www.dnd5eapi.co/api/proficiencies/68"
			}
		]
	},
	"languages": [
		{
			"name": "Common",
			"url": "http://www.dnd5eapi.co/api/languages/1"
		},
		{
			"name": "Dwarvish",
			"url": "http://www.dnd5eapi.co/api/languages/2"
		}
	],
	"language_desc": "You can speak, read, and write Common and Dwarvish. Dwarvish is full of hard consonants and guttural sounds, and those characteristics spill over into whatever other language a dwarf might speak.",
	"traits": [
		{
			"url": "http://www.dnd5eapi.co/api/traits/1",
			"name": "Darkvision (Dwarf)"
		},
		{
			"url": "http://www.dnd5eapi.co/api/traits/2",
			"name": "Dwarven Resilience"
		},
		{
			"url": "http://www.dnd5eapi.co/api/traits/3",
			"name": "Stonecunning"
		}
	],
	"subraces": [
		{
			"name": "Hill Dwarf",
			"url": "http://www.dnd5eapi.co/api/subraces/1"
		},
		{
			"name": "Mountain Dwarf",
			"url": "http://www.dnd5eapi.co/api/subraces/4"
		}
	],
	"url": "http://www.dnd5eapi.co/api/races/1"
}
 */