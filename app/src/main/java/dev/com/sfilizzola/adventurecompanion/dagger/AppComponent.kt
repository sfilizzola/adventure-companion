package dev.com.sfilizzola.adventurecompanion.dagger

import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import dev.com.sfilizzola.adventurecompanion.BaseApp
import dev.com.sfilizzola.adventurecompanion.dagger.modules.*
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidSupportInjectionModule::class),
    (NetworkModule::class),
    (ActivityModule::class),
    (FragmentModule::class),
    (DatabaseModule::class),
    (RepoModule::class),
    (ViewModelModule::class)])

interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: BaseApp): Builder

        fun build(): AppComponent
    }

    fun inject(application: BaseApp)
}