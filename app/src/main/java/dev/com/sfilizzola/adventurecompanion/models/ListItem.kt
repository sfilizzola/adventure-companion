package dev.com.sfilizzola.adventurecompanion.models

data class ListItem(var name:String, var id:Int)