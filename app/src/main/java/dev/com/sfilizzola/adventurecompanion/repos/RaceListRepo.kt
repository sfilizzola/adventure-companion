package dev.com.sfilizzola.adventurecompanion.repos


import dev.com.sfilizzola.adventurecompanion.models.ListItem
import dev.com.sfilizzola.adventurecompanion.models.Race
import dev.com.sfilizzola.adventurecompanion.network.NetworkClient
import dev.com.sfilizzola.adventurecompanion.utils.Utils
import io.reactivex.Flowable
import io.reactivex.Single

import javax.inject.Inject


class RaceListRepo @Inject constructor(private var service:NetworkClient){

    fun getRaces(): Single<List<ListItem>> {

        return service.listRaces().flatMapPublisher {
            Flowable.fromIterable(it.results)
        }.map {
            ListItem(it.name, Utils.getIdFromUrl(it.url))
        }.toList()

    }

    fun getRace(raceID:String):Single<Race>{
        return service.getRace(raceID).map {
            Race(it.index, it.name, it.speed, it.abilityBonuses, it.alignment,
                    it.age, it.size, it.sizeDesc, it.startingProficiences,
                    it.languages, it.traits, it.subraces, it.langDesc, it.url)
        }
    }
}