package dev.com.sfilizzola.adventurecompanion.network.networkModels

data class SimpleItemResponse (
        var name:String,
        var url:String
)