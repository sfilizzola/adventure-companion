package dev.com.sfilizzola.adventurecompanion.models


import android.os.Parcel
import android.os.Parcelable
import dev.com.sfilizzola.adventurecompanion.network.networkModels.ListResponse
import paperparcel.PaperParcel

@PaperParcel
data class Race(
        var index: Int,
        var name: String,
        var speed: Int,
        var abilityBonuses: ArrayList<Int>,
        var alignment: String,
        var age: String,
        var size: String,
        var sizeDesc: String,
        var startingProficiences: ArrayList<ListResponse>,
        var languages: ArrayList<ListResponse>,
        var traits: ArrayList<ListResponse>,
        var subraces: ArrayList<ListResponse>,
        var langDesc: String,
        var url: String) : Parcelable {
    companion object {
        @JvmField val CREATOR = PaperParcelRace.CREATOR
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        PaperParcelRace.writeToParcel(this, dest, flags)
    }
}