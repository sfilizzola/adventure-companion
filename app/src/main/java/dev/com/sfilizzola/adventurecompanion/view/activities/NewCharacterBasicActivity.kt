package dev.com.sfilizzola.adventurecompanion.view.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import dev.com.sfilizzola.adventurecompanion.R
import dev.com.sfilizzola.adventurecompanion.databinding.ActivityNewCharacterBasicBinding
import dev.com.sfilizzola.adventurecompanion.models.Character
import dev.com.sfilizzola.adventurecompanion.models.ListItem
import dev.com.sfilizzola.adventurecompanion.models.Race
import dev.com.sfilizzola.adventurecompanion.view.fragments.RaceCardFragment
import dev.com.sfilizzola.adventurecompanion.view.viewStatus.NewCharBasicStatus
import dev.com.sfilizzola.adventurecompanion.viewmodels.NewCharBasicViewModel
import timber.log.Timber
import javax.inject.Inject

class NewCharacterBasicActivity : BaseActivity(), AdapterView.OnItemSelectedListener {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(NewCharBasicViewModel::class.java) }

    private lateinit var binding: ActivityNewCharacterBasicBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_character_basic)
        binding.viewModel = viewModel

        //observer
        viewModel.getData().observe(this, Observer {
            it?.let { result ->
                when (result) {
                    is NewCharBasicStatus.RaceListLoaded -> {
                        fillSpinner(it.list())
                    }
                    is NewCharBasicStatus.RaceLoaded -> {
                        startRaceFragment(it.race())
                    }
                    is NewCharBasicStatus.Error -> {
                        Timber.e(it.error())
                    }
                    is NewCharBasicStatus.NextStep -> {
                        viewModel.generateNewChar(binding.newCharNameEdittext.text.toString())
                        changeActivity()
                    }
                }
            }
        })

    }





    private fun startRaceFragment(race: Race?) {
        val transaction = supportFragmentManager.beginTransaction()
        val fragment = RaceCardFragment()

        race?.let {
            val bundle = Bundle()
            bundle.putParcelable("race", it)
            fragment.arguments = bundle
        }

        transaction.replace(R.id.race_card,fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun fillSpinner(list: List<ListItem>) {

        val itemsTreated = viewModel.treatItemList(list)

        val adapter = ArrayAdapter<String>(this, R.layout.race_spinner_item, itemsTreated)

        binding.newCharRace.adapter = adapter

        binding.newCharRace.onItemSelectedListener = this


    }

    override fun onResume() {
        super.onResume()
        viewModel.fetchRaces()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        Timber.w("Nothing selected")
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        viewModel.fetchRace((position + 1).toString())
    }

    private fun changeActivity() {


    }
}