package dev.com.sfilizzola.adventurecompanion.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableInt
import android.view.View
import dev.com.sfilizzola.adventurecompanion.BaseApp
import dev.com.sfilizzola.adventurecompanion.models.Character
import dev.com.sfilizzola.adventurecompanion.models.ListItem
import dev.com.sfilizzola.adventurecompanion.repos.RaceListRepo
import dev.com.sfilizzola.adventurecompanion.view.viewStatus.NewCharBasicStatus
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class NewCharBasicViewModel @Inject constructor(var repo: RaceListRepo) : BaseViewModel() {

    private var data = MutableLiveData<NewCharBasicStatus>()

    var spinnerProgressVisibility = ObservableInt(View.GONE)
    var raceSpinnerVisibility = ObservableInt(View.VISIBLE)

    fun fetchRaces() {
        showRaceSpinnerLoad(true)
        compositeDisposable.add(repo.getRaces().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    data.postValue(NewCharBasicStatus.RaceListLoaded(it))
                    showRaceSpinnerLoad(false)
                }, {
                    data.postValue(NewCharBasicStatus.Error(it.message))
                    showRaceSpinnerLoad(false)
                }))
    }

    fun fetchRace(raceID: String) {
        compositeDisposable.add(repo.getRace(raceID).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    data.postValue(NewCharBasicStatus.RaceLoaded(it))
                }, {
                    data.postValue(NewCharBasicStatus.Error(it.message))
                }))
    }


    fun getData(): LiveData<NewCharBasicStatus> = data

    fun treatItemList(list: List<ListItem>): List<String> {

        val result = ArrayList<String>()
        for (item: ListItem in list) {
            result.add(item.name)
        }
        return result

    }

    fun showRaceSpinnerLoad(show:Boolean){
        if (show){
            spinnerProgressVisibility.set(View.VISIBLE)
            raceSpinnerVisibility.set(View.GONE)
        } else {
            spinnerProgressVisibility.set(View.GONE)
            raceSpinnerVisibility.set(View.VISIBLE)
        }
    }


    fun generateNewChar(charName:String){
        val newCharacter = Character(generateCurrentID(), charName)
        BaseApp.currentChar = newCharacter
    }

    private fun generateCurrentID(): Int {
        //TODO - get from database the next ID
        return 1
    }
}