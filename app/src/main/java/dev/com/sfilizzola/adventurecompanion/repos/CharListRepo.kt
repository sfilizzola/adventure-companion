package dev.com.sfilizzola.adventurecompanion.repos

import dev.com.sfilizzola.adventurecompanion.models.Character
import io.reactivex.Single

class CharListRepo {


    fun fetchCharList(): Single<List<Character>>{

        var charList = ArrayList<Character>()

        var char1 = Character(1, "Alton Highfoot")
        var char2 = Character(2, "Elandra Highfoot")
        var char3 = Character(3, "Darth Vader")
        var char4 = Character(4, "Luke Skywalker")

        charList.add(char1)
        charList.add(char2)
        charList.add(char3)
        charList.add(char4)

        return Single.just(charList)

    }
}