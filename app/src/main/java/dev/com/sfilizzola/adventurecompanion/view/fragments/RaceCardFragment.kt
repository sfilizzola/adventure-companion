package dev.com.sfilizzola.adventurecompanion.view.fragments

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dev.com.sfilizzola.adventurecompanion.R
import dev.com.sfilizzola.adventurecompanion.databinding.FragmentRaceCardBinding
import dev.com.sfilizzola.adventurecompanion.models.Race
import dev.com.sfilizzola.adventurecompanion.viewmodels.RaceFragmentViewModel
import javax.inject.Inject

class RaceCardFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(RaceFragmentViewModel::class.java) }

    private lateinit var binding: FragmentRaceCardBinding

    private lateinit var race:Race

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_race_card, container, false)
        binding.viewModel = viewModel

        race = arguments!!.getParcelable("race") as Race

        setUpCardLayout(race)

        return binding.root

    }

    private fun setUpCardLayout(race: Race) {
        binding.raceCardTitle.text = race.name
        binding.raceCardAgeValue.text = race.age
        binding.raceCardAlignmentValue.text = race.alignment
        binding.raceCardSizeDescValue.text = race.sizeDesc
        binding.raceCardSizeValue.text = race.size
        binding.raceCardSpeedValue.text = race.speed.toString()

    }
}