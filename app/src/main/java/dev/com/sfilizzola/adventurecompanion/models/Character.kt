package dev.com.sfilizzola.adventurecompanion.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import io.reactivex.annotations.NonNull

@Entity(tableName = "chars")
data class Character(
        @NonNull
        @PrimaryKey(autoGenerate = true)
        var id: Int,

        @NonNull
        var name: String) {

    var race: Race? = null
}

