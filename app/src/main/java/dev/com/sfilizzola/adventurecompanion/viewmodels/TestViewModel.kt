package dev.com.sfilizzola.adventurecompanion.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import dev.com.sfilizzola.adventurecompanion.models.ListItem
import dev.com.sfilizzola.adventurecompanion.repos.RaceListRepo
import timber.log.Timber
import javax.inject.Inject

class TestViewModel @Inject constructor(var repo: RaceListRepo) : BaseViewModel() {


    fun getCurrentText(): String {
        return "<- teste ->"
    }

    fun connectRaces(): LiveData<List<ListItem>> {

        val data = MutableLiveData<List<ListItem>>()

        compositeDisposable.add(repo.getRaces()   .subscribe({
            data.postValue(it)
        }, {
            Timber.e(it)
        }))

        return data
    }


}