package dev.com.sfilizzola.adventurecompanion.utils

import dev.com.sfilizzola.adventurecompanion.models.ListItem
import dev.com.sfilizzola.adventurecompanion.network.networkModels.ListResponse

class Utils {
    companion object {

        fun convertResponseToItem(response: ListResponse):MutableList<ListItem>{

            val convertedResponse = ArrayList<ListItem>()

            for (responseItem in response.results){
                var currentItem = ListItem(responseItem.name, getIdFromUrl(responseItem.url))
                convertedResponse.add(currentItem)
            }

            return convertedResponse
        }

        fun getIdFromUrl(url: String): Int {

            val splitedUrl = url.split("/")

            return splitedUrl.last().toInt()

        }
    }
}