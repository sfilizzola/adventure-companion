package dev.com.sfilizzola.adventurecompanion.network.networkModels

data class ListResponse(

        var count : Int,
        var results : MutableList<SimpleItemResponse>

)