package dev.com.sfilizzola.adventurecompanion.view.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import dev.com.sfilizzola.adventurecompanion.R
import dev.com.sfilizzola.adventurecompanion.databinding.ActivityTesteBinding
import dev.com.sfilizzola.adventurecompanion.models.ListItem
import dev.com.sfilizzola.adventurecompanion.viewmodels.TestViewModel
import timber.log.Timber
import javax.inject.Inject

class TestActivity : BaseActivity(), Observer<List<ListItem>> {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var binding: ActivityTesteBinding

    private val testViewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(TestViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_teste)
        binding.viewModel = testViewModel

        testViewModel.connectRaces().observe(this, this)

    }

    override fun onChanged(newContent: List<ListItem>?) {
        Timber.d(" ======> %s <========", newContent.toString())
    }
}