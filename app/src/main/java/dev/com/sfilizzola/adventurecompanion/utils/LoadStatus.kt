package dev.com.sfilizzola.adventurecompanion.utils

enum class LoadStatus {
    IDLE, LOADING, SUCCESS, ERROR
}